import React from 'react';
const UserContext = /*#__PURE__*/React.createContext({
  user: {},
  setUser: () => {}
});
export default UserContext;