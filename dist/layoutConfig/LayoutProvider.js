import React, { useState } from 'react';
import UserContext from './userContext';

const LayoutProvider = props => {
  const [state, setState] = useState({});
  return /*#__PURE__*/React.createElement(UserContext.Provider, {
    value: {
      user: state,
      setUser: user => {
        setState(user);
      }
    }
  }, props.children);
};

export default LayoutProvider;