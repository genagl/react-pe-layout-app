function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component, Fragment } from "react";
import { NavLink } from "react-router-dom";
import { existRoutingChilder, getFirstRoute } from "react-pe-layouts";
import { LayoutIcon } from 'react-pe-useful';
import { __ } from "react-pe-utilities";

class LayoutHelp extends Component {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "state", {});
  }

  render() {
    const route = getFirstRoute("help");
    const children = existRoutingChilder("help") ? getFirstRoute("help").children.map((ee, i) => {
      const rt = `/${route.route}/${ee.route}`;
      return /*#__PURE__*/React.createElement(NavLink, {
        to: rt,
        className: "podmenu",
        activeClassName: "active",
        key: i,
        route: rt
      }, __(ee.title));
    }) : null;
    return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(NavLink, {
      to: `/${route.route}`,
      className: "layout-header-help"
    }, /*#__PURE__*/React.createElement(LayoutIcon, {
      src: route.icon,
      className: "layout-header-icon"
    }), /*#__PURE__*/React.createElement("span", null, route.title)), children);
  }

}

export default LayoutHelp;