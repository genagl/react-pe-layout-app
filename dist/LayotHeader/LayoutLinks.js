function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component, Fragment } from "react";
import { link } from "react-pe-layouts";
import { LayoutIcon } from 'react-pe-useful';

class LayoutLinks extends Component {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "state", {});
  }

  render() {
    return link().map((e, i) => /*#__PURE__*/React.createElement("a", {
      href: e.route,
      target: "_blank",
      key: i,
      title: e.title,
      className: "layout-header-link",
      rel: "noreferrer"
    }, /*#__PURE__*/React.createElement(LayoutIcon, {
      src: e.icon,
      className: "layout-header-icon"
    }), /*#__PURE__*/React.createElement("span", null, e.title)));
  }

}

export default LayoutLinks;