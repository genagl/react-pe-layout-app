function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import $ from "jquery";
import { NavLink } from "react-router-dom";
import LayoutHeaderMenu from "./LayoutHeaderMenu";
import LayoutUser from "./LayoutUser";
import { __ } from "react-pe-utilities";
import { name, description } from "react-pe-layouts";
import { avatar, iconHeight, iconUrl, iconWidth, template } from "react-pe-layouts";
import { initArea } from "react-pe-utilities";
const components = {};

function importAll(r) {
  // console.log(r)
  r.keys().forEach(key => {
    const key1 = key.replace("./", "").split(".").slice(0, -1).join(".");
    components[key1] = r(key);
  });
}

importAll(require.context("widgets/", false, /\.js$/));

class LayoutHeader extends Component {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "state", {
      isOpen: false,
      height: 0,
      current: this.props.current,
      user: this.props.user,
      isHumburger: false,
      fixed: false
    });

    _defineProperty(this, "onHumburger", () => {
      let totalHeight = 0;
      console.log($("#layout-menu-right").children());

      if ($("#layout-menu-right") && $("#layout-menu-right").children() && $("#layout-menu-right").children().length > 0) {
        $("#layout-menu-right").children().map(child => {
          try {
            totalHeight += $(child).outerHeight(true); // true = include margins
          } catch (e) {}
        });
      }

      console.log(totalHeight);
      this.setState({
        isHumburger: !this.state.isHumburger,
        totalHeight: this.state.isHumburger ? totalHeight : 0
      });
    });
  }

  componentDidMount() {
    window.layoutHeader = this;
    window.addEventListener("scroll", this.onscrollHandler);
  }

  componentWillUnmount() {
    window.removeEventListener("scroll", this.onscrollHandler);
  }

  onscrollHandler(e) {
    return; // console.log( window.scrollY );
    // window.layoutHeader.setState(
    // 	{ fixed: window.scrollY > 86 && window.height > 540 },
    // 	() => {
    // 		if (window.layoutHeader.state.fixed) {
    // 			$("body").css("margin-top", 86)
    // 		} else {
    // 			$("body").css("margin-top", 0)
    // 		}
    // 	},
    // )
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.current !== this.state.current || nextProps.user !== this.state.user) {
      this.setState({
        current: nextProps.current,
        user: nextProps.user
      });
    }
  }

  render() {
    const fixedClass = this.state.fixed ? "layout-header fixed animation-opened animated2" : "layout-header";
    const widgets = template().layout_header && Array.isArray(template().layout_header) ? template().layout_header.map((e, i) => {
      switch (e.component) {
        case "NavLink":
          return /*#__PURE__*/React.createElement(NavLink, {
            key: i,
            className: `btn ${e.routing}`,
            to: `/${e.routing}`
          }, __(e.title));
          break;

        default:
          const _Widget = components[e.component].default;
          return /*#__PURE__*/React.createElement(_Widget, _extends({}, e, {
            key: i
          }));
      }
    }) : null;
    return /*#__PURE__*/React.createElement("div", {
      className: fixedClass
    }, /*#__PURE__*/React.createElement("div", {
      className: "layout-header-left"
    }, initArea("layout-left", { ...this.props
    }, /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(NavLink, {
      to: "/",
      style: {
        display: "flex"
      },
      className: "mainLink"
    }, /*#__PURE__*/React.createElement("div", {
      className: "layout-header-icon ",
      style: {
        backgroundImage: iconUrl(),
        height: iconHeight(),
        width: iconWidth()
      }
    }), /*#__PURE__*/React.createElement("div", {
      className: "layout-header-title"
    }, name(), /*#__PURE__*/React.createElement("div", {
      className: "layout-header-description"
    }, description()))), /*#__PURE__*/React.createElement("div", {
      className: "humburger",
      onClick: this.onHumburger
    }, /*#__PURE__*/React.createElement("i", {
      className: `fas ${this.state.isHumburger ? "fa-times" : "fa-bars"}`
    }))))), initArea("layout-header-center", { ...this.props
    }, /*#__PURE__*/React.createElement("div", {
      className: `d-flex-menu ${this.state.isHumburger ? "open" : ""}`
    }, /*#__PURE__*/React.createElement(LayoutHeaderMenu, {
      onHumburger: this.onHumburger,
      user: this.state.user
    }))), /*#__PURE__*/React.createElement("div", {
      className: `layout-menu-right ${this.state.isHumburger ? "open" : ""}`,
      id: "layout-menu-right"
    }, initArea("layout-header-right", { ...this.props
    }, /*#__PURE__*/React.createElement(LayoutUser, {
      current: this.state.current,
      onCurrent: this.props.onCurrent,
      user: this.state.user,
      refetchUser: this.props.refetchUser,
      avatar: avatar(),
      isOpen: false
    }))));
  }

}

export default LayoutHeader;