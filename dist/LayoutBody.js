function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import $ from "jquery";
import { Loading } from "react-pe-useful";
import { compose } from "recompose";
import { Query, withApollo } from "react-apollo";
import { withRouter } from "react-router";
import gql from "graphql-tag";
import { cssStyle, loginPage, template } from "react-pe-layouts";
import LayoutContent from "./LayoutContent";
import LayoutHeader from "./LayotHeader";
import LayoutFooter from "./LayoutFooter";
import { __, initWidgets } from "react-pe-utilities";
import { title } from "react-pe-layouts";
import { queryUserInfo } from "react-pe-layouts";
import { isLoggedPage } from "react-pe-utilities";
import { initArea } from "react-pe-utilities";
import { updateRoutes, updateApp, updateWidgets, updateTemplate, layoutInit } from "react-pe-layouts";
import UserContext from "./layoutConfig/userContext";

class LayoutBody extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "random", void 0);

    _defineProperty(this, "onChangeStyle", style => {
      // console.log( style );
      localStorage.setItem("css", style.style);
      localStorage.setItem("fluid", parseInt(style.fluid) ? 1 : 0);
      $("#external-css").detach();

      if (style.style) {
        // console.log( style.style, this.state.style );
        $("#under-footer").append(`<link rel="stylesheet" type="text/css" href=${style.style}?${this.random} id="external-css"/>`);
      } else {} // this.setState( { style } );
      // console.log( localStorage.getItem("fluid"));

    });

    _defineProperty(this, "onCurrent", i => {
      this.setState({
        current: i
      });
    });

    this.random = Math.random();
    const fl = localStorage.getItem("fluid");
    const fluid = typeof fl !== "undefined" ? fl : 1;
    const _style = {
      style: cssStyle(),
      fluid
    }; // console.log(style, fl);

    document.title = title();
    const token = localStorage.getItem("token");

    if (!token && isLoggedPage(props.location.pathname)) {
      this.props.history.push(loginPage());
    }

    this.state = {
      current: 100,
      style: _style,
      fluid,
      isLoad: false
    }; //
  }

  componentDidMount() {}

  renderContent(context) {
    //console.log(this.props)
    const query = queryUserInfo();
    const queryMenu = gql`
			query
			{
			  getInit
			  {
				menu
				{
				  	json
				}
				widgets
				{
					json
				}
				public_options 
				{
					json 
				}
				template 
				{
					json 
				}
			  }
			}`;
    return /*#__PURE__*/React.createElement(Query, {
      query: queryMenu
    }, ret => {
      if (ret.loading) {
        return /*#__PURE__*/React.createElement(Loading, null);
      }

      if (ret.data) {
        // console.log( ret.data.getInit.menu.json );
        updateRoutes(ret.data.getInit.menu.json ? JSON.parse(ret.data.getInit.menu.json) : {});
        updateApp(JSON.parse(ret.data.getInit.public_options.json.replace(/'/g, '"')));

        try {
          updateWidgets(ret.data.getInit.widgets.json ? JSON.parse(ret.data.getInit.widgets.json) : {});
          initWidgets();
        } catch (e) {
          console.log(e.message, ": ", ret.data.getInit.widgets);
        }

        try {
          updateTemplate(JSON.parse(ret.data.getInit.template.json));
        } catch (e) {
          console.log(e.message);
        }

        return /*#__PURE__*/React.createElement(Query, {
          query: query
        }, ({
          loading,
          error,
          data,
          refetch,
          previousData
        }) => {
          if (loading) {
            return /*#__PURE__*/React.createElement(Loading, null);
          }

          if (data) {
            let user = null;

            if (data.userInfo) {
              if (data.userInfo.user) {
                user = data.userInfo.user;
              } else {
                user = data.userInfo;
              }
            }

            if (!error && typeof previousData === 'undefined') {
              context.setUser(user);
            }

            if (error) {
              console.log(error);
            } //console.log(template())


            return /*#__PURE__*/React.createElement(React.Fragment, null, initArea("layout-app", { ...this.props,
              ...this.state,
              user,
              refetchUser: refetch
            }, /*#__PURE__*/React.createElement("div", {
              className: "layout block w-100"
            }, !template().header ? null : initArea("layout-header", { ...this.props,
              ...this.state,
              user,
              refetchUser: refetch,
              onCurrent: this.onCurrent
            }, /*#__PURE__*/React.createElement(LayoutHeader, {
              name: this.props.name,
              current: this.state.current,
              onCurrent: this.onCurrent,
              user: user,
              refetchUser: refetch
            })), /*#__PURE__*/React.createElement(LayoutContent, {
              current: this.state.current,
              onCurrent: this.onCurrent,
              user: user,
              onChangeStyle2: style => this.onChangeStyle(cssStyle()),
              onChangeStyle: this.onChangeStyle,
              refetchUser: refetch
            }), /*#__PURE__*/React.createElement(LayoutFooter, null))));
          }

          if (error) {
            if (localStorage.getItem("token")) {
              localStorage.removeItem("token", null);
              window.location.reload();
            }

            console.log(error);
            return /*#__PURE__*/React.createElement("div", {
              className: "media w-100 lead  "
            }, /*#__PURE__*/React.createElement("div", {
              className: "fatal-error"
            }), /*#__PURE__*/React.createElement("div", {
              className: "fatal-text"
            }, __("If you see this inscription, something wrong happened: critical errors occurred on our server. We dare to assure you that our experts have already pulled on their space suits and are already poking around in orbit. So soon everything will be OK!")));
          }
        });
      }

      if (ret.error) {
        return /*#__PURE__*/React.createElement("div", {
          className: "media w-100 lead  "
        }, /*#__PURE__*/React.createElement("div", {
          className: "fatal-error"
        }), /*#__PURE__*/React.createElement("div", {
          className: "fatal-text"
        }, __("If you see this inscription, something wrong happened: critical errors occurred on our server. We dare to assure you that our experts have already pulled on their space suits and are already poking around in orbit. So soon everything will be OK!")));
      }
    });
  }

  render() {
    const cl = this.props.location.pathname.split("/").splice(1).map(e => `route-${e}`).join(" ");
    const clss = this.state.style && this.state.style.fluid ? "container-fluid  cont" : "container cont";
    return /*#__PURE__*/React.createElement("div", {
      className: `full ${cl}`
    }, /*#__PURE__*/React.createElement("header", null), /*#__PURE__*/React.createElement("main", null, /*#__PURE__*/React.createElement("div", {
      className: clss
    }, /*#__PURE__*/React.createElement(UserContext.Consumer, null, context => this.renderContent(context)))), /*#__PURE__*/React.createElement("footer", null), /*#__PURE__*/React.createElement("div", {
      id: "under-footer"
    }, /*#__PURE__*/React.createElement("link", {
      href: `/assets/css/style.css?${this.random}`,
      rel: "stylesheet"
    }), /*#__PURE__*/React.createElement("link", {
      rel: "stylesheet",
      type: "text/css",
      href: `${cssStyle()}?${this.random}`,
      id: "external-css"
    })));
  }

}

export default compose( // graphql(l_token, {"name": "token"}),
withApollo, withRouter)(LayoutBody);