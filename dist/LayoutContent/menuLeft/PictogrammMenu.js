function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { withRouter } from "react-router";
import $ from "jquery";
import { LayoutIcon } from 'react-pe-useful';
import { __ } from "react-pe-utilities"; //import { isCapability } from "../../../layouts/user"

import { initArea } from "react-pe-utilities";

class PictogrammMenu extends Component {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "state", {
      current: this.props.current,
      hover: false
    });
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      current: nextProps.current
    });
  }

  render() {
    const {
      children,
      route,
      icon,
      title
    } = this.props; // const isRole = isCapability( capability);
    // if(isRole) return "";

    const arrow = children && children.length > 0 ? /*#__PURE__*/React.createElement("span", {
      className: "arrow fas fa-caret-right"
    }) : null;
    return /*#__PURE__*/React.createElement("div", {
      className: `position-relative pictogramm-menu-element${this.props.i === this.state.current ? " active" : ""}`
    }, /*#__PURE__*/React.createElement(NavLink, {
      to: `/${route}`,
      exact: true,
      strict: true,
      className: `layout-left-btn ${this.props.location.pathname === `/${route}` ? "active" : ""}`,
      route: route,
      isActive: match => {
        setTimeout(() => {
          const offset = $(".layout-left-btn.active").offset();
          $("#mobile-bar").offset({
            top: offset ? offset.top : 65
          });
        }, 50);

        if (match) {// console.log(match);
          // console.log(location);
          // console.log(this.props.location);
          // console.log(this.props.match);
        }
      },
      activeClassName: "active"
    }, /*#__PURE__*/React.createElement("div", {
      className: "layout-menu-icon"
    }, /*#__PURE__*/React.createElement(LayoutIcon, {
      src: icon,
      className: "left-menu-icon"
    })), /*#__PURE__*/React.createElement("div", {
      className: `layout-menu-left-label ${this.state.hover ? "hover" : null}`
    }, __(title))), arrow, initArea("menu-left-element", { ...this.props,
      data: { ...this.props
      },
      level: 0,
      pathname: `/${route}`,
      i: this.props.i,
      state: this.state
    }));
  }

  toggleHover() {
    this.setState({
      hover: !this.state.hover
    });
  }

}

export default withRouter(PictogrammMenu);