function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { graphql, compose } from "react-apollo";
import { NavLink } from "react-router-dom";
import { Intent, Tag } from "./utilities/AppToaster";
import { i18n, Loading } from "react-pe-utilities";
import leftmenu from "../../states/mapState/graphql/leftmenu.graphql";
import main_request from "../../states/mapState/graphql/get_user_request.graphql";
import request_verify from "../../states/mapState/graphql/request_verify.graphql";

class LeftMenuWithVerify extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "onNewVerify", () => {
      this.props.request_verify({
        variables: {
          url: window.location.origin
        },
        update: (store, {
          data: {
            request_verify
          }
        }) => {
          console.log(request_verify);
        }
      });
    });

    this.state = {};
  }

  render() {
    if (this.props.data.loading) return /*#__PURE__*/React.createElement(Loading, null);
    let leftmenu = this.props.data.leftmenu.left_menu || [];
    let comment = /*#__PURE__*/React.createElement("div", {
      className: "lead px-3 py-3 text-light text-center ",
      style: {
        backgroundColor: "#182026"
      }
    }, i18n.t("Personal Cabinet"));

    if (this.props.init.user.account_activated == 0) {
      leftmenu = leftmenu.filter(elem => elem.id == "params");
      comment = /*#__PURE__*/React.createElement(React.Fragment, null, comment, /*#__PURE__*/React.createElement(Tag, {
        icon: false,
        intent: Intent.DANGER,
        minimal: false,
        fill: true,
        round: false,
        large: true,
        className: "p-4 text-center",
        style: {
          height: 200
        }
      }, /*#__PURE__*/React.createElement("div", {
        className: "mb-3"
      }, `	${i18n.t("Verify your e-mail for full functionality.")}`), /*#__PURE__*/React.createElement("div", {
        className: "small btn btn-outline-light btn-sm px-3 py-1",
        onClick: this.onNewVerify
      }, i18n.t("Request new verification"))));
    } // console.log( this.props );
    // const main_request = this.props.main_request.main_request || {};
    // const requests = main_request.requests || {};


    const menus = leftmenu.map((menuPoint, num) => {
      const alert = menuPoint.alert ? /*#__PURE__*/React.createElement("div", {
        className: "indic",
        title: menuPoint.alert.hint
      }, menuPoint.alert.label) : null;
      const success = menuPoint.success ? /*#__PURE__*/React.createElement("div", {
        className: "indicm",
        title: menuPoint.success.hint
      }, menuPoint.success.label) : null;
      return menuPoint.id === "separator" ? /*#__PURE__*/React.createElement("li", {
        className: "separator",
        key: num
      }, /*#__PURE__*/React.createElement("div", {
        className: "spacer-5"
      })) : /*#__PURE__*/React.createElement("li", {
        className: num === this.state.num ? "active" : num,
        key: num
      }, /*#__PURE__*/React.createElement(NavLink, {
        to: `/cabinet/${menuPoint.id}`,
        lid: menuPoint.id,
        activeClassName: "active"
      }, /*#__PURE__*/React.createElement("span", null, /*#__PURE__*/React.createElement("i", {
        className: menuPoint.icon
      })), i18n.t(menuPoint.title), /*#__PURE__*/React.createElement("i", {
        className: "fas fa-angle-right zz"
      }, " "), success, alert));
    });
    return /*#__PURE__*/React.createElement(React.Fragment, null, comment, /*#__PURE__*/React.createElement("ul", {
      className: "bio_cab_menu"
    }, menus));
  }

} //


LeftMenuWithVerify.propTypes = {};
LeftMenuWithVerify.defaultProps = {}; // (props.match.params.id ? props.match.params.id : "")

export default compose(graphql(leftmenu), graphql(main_request, {
  name: "main_request"
}), graphql(request_verify, {
  name: "request_verify"
}))(LeftMenuWithVerify); // https://github.com/apollographql/react-apollo/issues/660
// https://reacttraining.com/react-router/web/api/NavLink