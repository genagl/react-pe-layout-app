function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { NavLink, withRouter } from "react-router-dom";
import { menu } from "react-pe-layouts";
import { template } from "react-pe-layouts";
import { isCapability } from "react-pe-utilities";
import PictogrammMenu from "./menuLeft/PictogrammMenu";
import HierarhicalMenu from "./menuLeft/HierarhicalMenu";
import { initArea } from "react-pe-utilities";

class LayoutMenuLeft extends Component {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "state", {
      current: this.props.current,
      hover: false,
      show: true
    });

    _defineProperty(this, "toggle", () => {
      this.setState({
        show: !this.state.show
      });
    });

    _defineProperty(this, "onSwitch", evt => {
      // this.setState({ current:evt.currentTarget.getAttribute("i") });
      this.props.onCurrent(evt.currentTarget.getAttribute("i"));
    });
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      current: nextProps.current
    });
  }

  render() {
    const icons = menu().menu.map((e, i) => {
      const islogged = (e.islogged === true || e.islogged === 1) && !this.props.user;
      if (islogged) return "";
      const isRole = isCapability(e.capability, this.props.user);
      if (isRole) return "";

      switch (template().left_menu) {
        case "pictogramm":
          return /*#__PURE__*/React.createElement(PictogrammMenu, _extends({}, e, {
            current: this.state.current,
            i: i,
            key: i
          }));

        case "hierarhical":
        default:
          return /*#__PURE__*/React.createElement("div", {
            key: i
          }, /*#__PURE__*/React.createElement("div", {
            className: " left-menu-group " + (this.state.show ? "d-block" : "hidden")
          }, /*#__PURE__*/React.createElement(HierarhicalMenu, _extends({}, e, {
            parent_route: "",
            razdel: e.children ? e.children : [e],
            i: i,
            level: 1,
            key: i
          }))));
      }
    });
    return /*#__PURE__*/React.createElement("div", {
      className: "layout-menu-left " + template().left_menu
    }, /*#__PURE__*/React.createElement("button", {
      className: "admin-menu-toggle",
      onClick: this.toggle
    }), initArea("layout-before-left-menu", { ...this.props,
      ...this.state
    }), icons, initArea("layout-after-left-menu", { ...this.props,
      ...this.state
    }), /*#__PURE__*/React.createElement("div", {
      className: "layout-settings-btn "
    }, /*#__PURE__*/React.createElement(NavLink, {
      to: "/cog",
      className: "layout-left-btn ",
      activeClassName: "active"
    }, /*#__PURE__*/React.createElement("div", {
      className: "layout-menu-icon"
    }, /*#__PURE__*/React.createElement("i", {
      className: "fas fa-cog"
    })))));
  }

}

export default withRouter(LayoutMenuLeft);